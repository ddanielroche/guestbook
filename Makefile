SHELL := /bin/bash

tests:
	symfony console doctrine:migrations:migrate
	symfony console doctrine:fixtures:load -n
	symfony php bin/phpunit
.PHONY: tests
